<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>Comments</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript">
	function del()
	{
		if(confirm("你真的想删除该记录么？"))
		{
			return true;
		}
		return false;
	}
	</script>
  </head>
  
  <body>
    <h2>Application</h2>
    <table border="1" width="95%" align="center" style="font: 12px">
    	<tr>
    		<td>ID</td>
    		<td>Application Name</td>
    	</tr>
    	<tr>
    		<td>${c.appId}</td>
    		<td>${appInfo.name}</td>
    	</tr>
    </table>
    <h2>Comments</h2>
    <table border="1" width="95%" align="center" style="font: 12px">
    	<tr>
    		<td>ID</td>
    		<td>Content</td>
    		<td>Time</td>
    		<td>Sender</td>
    		<td>Operation</td>
    	</tr>
	    <s:iterator value="#request.ListComment.list" id="lc">
	    <tr>
    		<td><s:property value="#lc.id" /></td>
    		<td><s:property value="#lc.content" /></td>
    		<td><s:date name="#lc.commentTime" format="yyyy-MM-dd"/></td>
    		<td><s:property value="#lc.userName" /></td>
    		<td><s:a href="removeComment.action?c.id=%{#lc.id}&c.appId=%{#lc.appId}" onclick="return del();">Delete</s:a></td>
    	</tr>
	    </s:iterator>
	    <tr>
				<td colspan="5">
					<s:if test="%{pageBean.currentPage == 1}">   
			　　　　　　第一页 上一页   
			　　　　</s:if>   
			　　　　<s:else>   
			　　　　　　<a href="listComment.action?page=1&c.appId=${c.appId}">第一页</a>   
			　　　　　　<a href="listComment.action?page=<s:property value="%{pageBean.currentPage-1}"/>&c.appId=${c.appId}">上一页</a>   
			　　　　</s:else>   
			　　　　<s:if test="%{pageBean.currentPage < pageBean.totalPage}">   
			　　　　　　<a href="listComment.action?page=<s:property value="%{pageBean.currentPage+1}"/>&c.appId=${c.appId}">下一页</a>   
			　　　　　　<a href="listComment.action?page=<s:property value="pageBean.totalPage"/>&c.appId=${c.appId}">最后一页</a>   
			　　　　</s:if>   
			　　　　<s:else>   
			　　　　　　下一页 最后一页   
			　　　　</s:else>
				</td>
			</tr>
    </table>
    <h2>Add Comment</h2>
    <s:form action="/admin/saveComment" method="post">
    	<s:hidden name="c.appId" value="%{c.appId}"></s:hidden>
    	<s:textfield name="c.userName" value="Name" />
		<s:textarea name="c.content" />
		<s:submit></s:submit>
    </s:form>
  </body>
</html>
