package com.struts2.obj;

/**
 * Appsigntable entity. @author MyEclipse Persistence Tools
 */

public class Appsign implements java.io.Serializable {

	// Fields

	private Integer appSignId;
	private Integer signId;
	private Integer appId;

	// Constructors

	/** default constructor */
	public Appsign() {
	}

	/** full constructor */
	public Appsign(Integer signId, Integer appId) {
		this.signId = signId;
		this.appId = appId;
	}

	// Property accessors

	public Integer getAppSignId() {
		return this.appSignId;
	}

	public void setAppSignId(Integer appSignId) {
		this.appSignId = appSignId;
	}

	public Integer getSignId() {
		return this.signId;
	}

	public void setSignId(Integer signId) {
		this.signId = signId;
	}

	public Integer getAppId() {
		return this.appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

}