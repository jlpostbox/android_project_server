package com.struts2.service;

import java.util.List;

import com.struts2.obj.Appinfo;
import com.struts2.obj.PageBean;

public interface AppInfoService {

	public void saveApp(Appinfo appinfo, Integer systemId, String appId);

	public void removeApp(Appinfo appinfo);

	public void updateApp(Appinfo appinfo);

	public List<Appinfo> findAllApps();

	public Appinfo findAppById(Integer id);

	/**
	 * 分页查询
	 * @param currentPage当前第几页
	 * @param pageSize每页大小
	 * @return 封闭了分页信息(包括记录集list)的Bean
	 */
	public PageBean queryForPage(int pageSize, int currentPage);
	
	/**
	 * 分页查询
	 * @param currentPage当前第几页
	 * @param pageSize每页大小
	 * @return 封闭了分页信息(包括记录集list)的Bean
	 */
	public PageBean queryForPageBySign(int pageSize, int currentPage , int signId);

	public void updateView(int appID);
	
	public PageBean queryForPageOrderByView(int pageSize, int currentPage);
	
	public PageBean search(String key,int pageSize, int page);
}
