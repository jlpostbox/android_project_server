package com.struts2.dao;

import java.util.List;

import com.struts2.obj.Appinfo;

public interface AppInfoDAO {
	
	public void saveApp(Appinfo appinfo);
	
	public void updateApp(Appinfo appinfo);
	
	public void removeApp(Appinfo appinfo);
	
	public List<Appinfo> findAllApps();
	
	public Appinfo findAppById(Integer id);
		
	/**
	* 分页查询 
　　 * @param hql 查询的条件 
　　 * @param offset 开始记录 
　　 * @param length 一次查询几条记录 
　　 * @return 
　　 */  
	public List<Appinfo> queryForPage(final String hql , final int offset,final int length);
	
	/**
	* 查询所有记录数 
	* @param hql 查询的条件 
	* @return 总记录数 
	*/  
	public int getAllRowCount(String hql);
	
	/**
	 * 更新浏览量
	 * @param appID
	 */
	public void updateView(int appID);
	
	
}
