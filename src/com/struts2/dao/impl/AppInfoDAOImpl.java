package com.struts2.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.struts2.dao.AppInfoDAO;
import com.struts2.obj.Appinfo;

@SuppressWarnings("unchecked")
public class AppInfoDAOImpl extends HibernateDaoSupport implements AppInfoDAO {
	@Override
	public List<Appinfo> findAllApps() {
		String hql = "from Appinfo order by id desc";
		return (List<Appinfo>) this.getHibernateTemplate().find(hql);
	}

	@Override
	public Appinfo findAppById(Integer id) {
		return (Appinfo) this.getHibernateTemplate().get(Appinfo.class, id);
	}

	@Override
	public void removeApp(Appinfo appinfo) {
		this.getHibernateTemplate().delete(appinfo);

	}

	@Override
	public void saveApp(Appinfo appinfo) {
		this.getHibernateTemplate().save(appinfo);
	}

	@Override
	public List<Appinfo> queryForPage(final String hql, final int offset,
			final int length) {
		List<Appinfo> appinfolist = getHibernateTemplate().executeFind(
				new HibernateCallback() {
					public Object doInHibernate(Session session)
							throws HibernateException, SQLException {
						Query query = session.createQuery(hql);
						query.setFirstResult(offset);
						query.setMaxResults(length);
						List list = query.list();
						return list;
					}
				});
		return appinfolist;
	}
	
	@Override
	public int getAllRowCount(String hql) {
		
		return getHibernateTemplate().find(hql).size();
	}

	@Override
	public void updateApp(Appinfo appinfo) {
		this.getHibernateTemplate().saveOrUpdate(appinfo);
	}

	@Override
	public void updateView(int appID) {
		Appinfo a = (Appinfo) this.getHibernateTemplate().get(Appinfo.class, appID);
		a.setAppView(a.getAppView() + 1);
		this.getHibernateTemplate().saveOrUpdate(a);
		
	}
	
}
