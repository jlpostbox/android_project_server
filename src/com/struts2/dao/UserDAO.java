package com.struts2.dao;

import java.util.List;

import com.struts2.obj.User;

public interface UserDAO {

	public void saveUser(User user);
		
	public void updateUser(User user);
	
	public void removeUser(User user);
	
	public List<User> findAllUsers();
	
	public User findUserById(Integer id);
}
