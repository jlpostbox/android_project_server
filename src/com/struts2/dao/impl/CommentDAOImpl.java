package com.struts2.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.struts2.dao.CommentDAO;
import com.struts2.obj.Comment;

public class CommentDAOImpl extends HibernateDaoSupport implements CommentDAO {

	@Override
	public void removeComment(Comment c) {
		this.getHibernateTemplate().delete(c);

	}

	@Override
	public void saveComment(Comment c) {
		this.getHibernateTemplate().save(c);

	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Comment> findByAppID(Integer id) {
		String hql = "from Comment where appId = " + id;
		return (List<Comment>)this.getHibernateTemplate().find(hql);
	}

	@Override
	public int getAllRowCount(final String hql) {
		return getHibernateTemplate().find(hql).size();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Comment> queryForPage(final int offset, final int length , final String hql) {
		List<Comment> commentlist = getHibernateTemplate().executeFind(new HibernateCallback(){   
					public Object doInHibernate(Session session) throws HibernateException,SQLException{   
						Query query = session.createQuery(hql);   
						query.setFirstResult(offset);   
						query.setMaxResults(length);   
						List list = query.list();
						return list;
					}   
				});
		return commentlist;
	}

}
