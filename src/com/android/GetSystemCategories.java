package com.android;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.struts2.obj.PageBean;
import com.struts2.service.AppSignService;

public class GetSystemCategories extends HttpServlet {

	private AppSignService appSignService;
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter pw=null; 
		pw = (PrintWriter)response.getWriter(); 
		
		int page = Integer.parseInt(request.getParameter("page"));
		//String x = new String(param.getBytes("ISO-8859-1"), "utf-8"); 
		
		ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		appSignService = (AppSignService) ctx.getBean("appSignService");
		
		PageBean pageBean = appSignService.getAllSystemSign(10, page);
		
		//JSONArray jsonArray = JSONArray.fromObject(pageBean);
		JSONObject jsonObject = JSONObject.fromObject(pageBean);
		System.out.println(jsonObject);
		
		pw.print(jsonObject);
	}

}
