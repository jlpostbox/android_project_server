package com.struts2.service.impl;

import java.util.List;

import com.struts2.dao.SignDAO;
import com.struts2.obj.Sign;
import com.struts2.service.SignService;

public class SignServiceImpl implements SignService {

	private SignDAO signdao;
	
	public SignDAO getSigndao() {
		return signdao;
	}

	public void setSigndao(SignDAO signdao) {
		this.signdao = signdao;
	}

	@Override
	public List<Sign> findAllSigns() {
		return signdao.findAllSigns();
	}

	@Override
	public Sign findSignById(Integer id) {
		return signdao.findSignById(id);
	}

	@Override
	public List<Sign> findSignBySignType(String signType) {
		return signdao.findSignBySignType(signType);
	}

	@Override
	public List<Sign> findSignByUserId(Integer id) {
		return signdao.findSignByUserId(id);
	}

	@Override
	public void removeSign(Sign sign) {
		signdao.removeSign(sign);

	}

	@Override
	public void saveSign(Sign sign) {
		signdao.saveSign(sign);

	}

	@Override
	public void updateSign(Sign sign) {
		signdao.updateSign(sign);

	}

}
