<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<title>应用程序</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
		<script type="text/javascript">
	function del()
	{
		if(confirm("你真的想删除该记录么？"))
		{
			return true;
		}
		return false;
	}
	</script>
	</head>

	<body>
		<h2>
			添加应用
		</h2>
		<s:fielderror />
		<s:form action="/admin/addApps" method="post"
			enctype="multipart/form-data">
			<s:textfield name="appInfo.name" value="软件名"
				label="%{getText('appName')}" />
			<s:textarea name="appInfo.appDescription" value="软件描述"
				label="%{getText('appDescription')}" />
			<s:textfield name="appInfo.appDownAddress" value="软件下载地址"
				label="%{getText('appDownAddress')}"></s:textfield>
			<s:textfield name="appInfo.appType" value="软件类型"
				label="%{getText('appType')}"></s:textfield>
			<s:textfield name="appInfo.appUseEnvironment" value="运行环境"
				label="%{getText('userEnvironment')}"></s:textfield>
			<s:textfield name="appInfo.appSystemRequest" value="系统要求"
				label="%{getText('appSystemRequest')}"></s:textfield>
			<s:file name="appImgFile" label="%{getText('appImg')}" />

			<s:radio name="systemId" list="#request.ListSignType0" listKey="id"
				listValue="name" label="%{getText('systemSign')}" />
			<s:checkboxlist name="appId" list="#request.ListSignType1"
				listKey="id" listValue="name" label="%{getText('appSign')}" />
			<s:submit></s:submit>
		</s:form>

		<h1>
			All Application
		</h1>

		<table border="1" width="100%" align="center" style="font-size: 12px; text-align:center">
			<tr>
				<td width="3%">
					序号
				</td>
				<td width="10%">
					软件名
				</td>
				<td width="30%">
					描述
				</td>
				<td width="5%">
					下载地址
				</td>
				<td>
					软件类型
				</td>
				<td>
					运行环境
				</td>
				<td>
					系统要求
				</td>
				<td>
					上传时间
				</td>
				<td width="4%">
					浏览量
				</td>
				<td width="4%">
					回复量
				</td>
				<td width="4%">
					上传人
				</td>
				<td width="5%">
					系统标签
				</td>
				<td width="5%">
					软件标签
				</td>
				<td width="7%">
					操作
				</td>
			</tr>
			<s:iterator value="#request.ListApps.list" id="app" status="appIndex">
				<tr>
					<td>
						<s:property value="#app.id" />
					</td>
					<td>
						<img src="../UploadImages/<s:property value='#app.appImg' />" width='50' /><br/>
						<s:property value="#app.name" />
					</td>
					<td>
						<s:property value="#app.appDescription" />
					</td>
					<td>
						<s:property value="#app.appDownAddress" />
					</td>
					<td>
						<s:property value="#app.appType" />
					</td>
					<td>
						<s:property value="#app.appUseEnvironment" />
					</td>
					<td>
						<s:property value="#app.appSystemRequest" />
					</td>
					<td>
						<s:date name="#app.appUploadTime" format="yyyy-MM-dd"/>
					</td>
					<td>
						<s:property value="#app.appView" />
					</td>
					<td>
						<s:property value="#app.appComment" />
					</td>
					<td>
						<s:property value="#app.userId" />
					</td>
					<td>
						<s:iterator value="#app.sign" id="sign">
							<s:if test="#sign.signType == 0">
								<s:property value="#sign.name" />
							</s:if>
						</s:iterator>
					</td>
					<td>
						<s:iterator value="#app.sign" id="sign">
							<s:if test="#sign.signType == 1">
								<s:a href="listBySignApps.action?signId=%{#sign.id}&page=1"><s:property value="#sign.name" /></s:a>
							</s:if>
						</s:iterator>
						
					</td>
					<td>
						<s:a href="showApps.action?appInfo.id=%{#app.id}">更新</s:a>&nbsp;
						<s:a href="deleteApps.action?appInfo.id=%{#app.id}"
							onclick="return del();">删除</s:a><br/>
						<s:a href="listComment.action?c.appId=%{#app.id}">评论</s:a>&nbsp;
						<s:a href="listImage.action?i.appId=%{#app.id}">图片</s:a>
					</td>
				</tr>
			</s:iterator>
			<tr>
				<td colspan="15">
					<s:if test="%{pageBean.currentPage == 1}">   
			　　　　　　第一页 上一页   
			　　　　</s:if>   
			　　　　<s:else>   
			　　　　　　<a href="listApps.action?page=1">第一页</a>   
			　　　　　　<a href="listApps.action?page=<s:property value="%{pageBean.currentPage-1}"/>">上一页</a>   
			　　　　</s:else>   
			　　　　<s:if test="%{pageBean.currentPage < pageBean.totalPage}">   
			　　　　　　<a href="listApps.action?page=<s:property value="%{pageBean.currentPage+1}"/>">下一页</a>   
			　　　　　　<a href="listApps.action?page=<s:property value="pageBean.totalPage"/>">最后一页</a>   
			　　　　</s:if>   
			　　　　<s:else>   
			　　　　　　下一页 最后一页   
			　　　　</s:else>
				</td>
			</tr>
		</table>
 
	</body>
</html>
