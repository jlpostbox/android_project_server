package com.struts2.service;

import java.util.List;

import com.struts2.obj.Image;

public interface ImageService {
	
	public void saveImage(Image i);

	public void removeImage(Image i);
	
	public List<Image> listImage(Integer appId);
}
