package com.struts2.action;

import java.io.File;
import java.util.Map;
import java.util.UUID;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.struts2.obj.Image;
import com.struts2.service.ImageService;
import com.tool.Utils;

public class ImageAction extends ActionSupport {

	private ImageService imageService;
	private Image i;
	
	private File[] uploadFile;
	private String[] uploadFileContentType;
	private String[] uploadFileFileName;
	
	public File[] getUploadFile() {
		return uploadFile;
	}
	public void setUploadFile(File[] uploadFile) {
		this.uploadFile = uploadFile;
	}
	public String[] getUploadFileContentType() {
		return uploadFileContentType;
	}
	public void setUploadFileContentType(String[] uploadFileContentType) {
		this.uploadFileContentType = uploadFileContentType;
	}
	public String[] getUploadFileFileName() {
		return uploadFileFileName;
	}
	public void setUploadFileFileName(String[] uploadFileFileName) {
		this.uploadFileFileName = uploadFileFileName;
	}
	
	public ImageService getImageService() {
		return imageService;
	}
	public void setImageService(ImageService imageService) {
		this.imageService = imageService;
	}
	public Image getI() {
		return i;
	}
	public void setI(Image i) {
		this.i = i;
	}
	
	public String remove() throws Exception {
		imageService.removeImage(i);
		return "removeSuccess";
	}
	
	public String save() throws Exception {

		 for (int j = 0; j < this.getUploadFile().length; j++) {
			 String imageFileName = UUID.randomUUID() + Utils.getExtention(this.getUploadFileFileName()[j]);
			 i.setName(imageFileName);
			 File imageFile = new File(ServletActionContext.getServletContext().getRealPath("UploadImages") + "/" + imageFileName);
			 Utils.copy(this.getUploadFile()[j], imageFile);			 
			 imageService.saveImage(i);
		}
		return "saveSuccess";
	}
	
	@SuppressWarnings("unchecked")
	public String list() throws Exception{
		Map request = (Map)ActionContext.getContext().get("request");
		request.put("ListImage", imageService.listImage(i.getAppId()));
		return "listSuccess";
	}
}
