package com.tool;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
	
	private static final int BUFFER_SIZE = 16 * 1024 ;

	public static void copy(File src, File dst) {
        try {
           InputStream in = null ;
           OutputStream out = null ;
            try {                
               in = new BufferedInputStream( new FileInputStream(src), BUFFER_SIZE);
               out = new BufferedOutputStream( new FileOutputStream(dst), BUFFER_SIZE);
                byte [] buffer = new byte [BUFFER_SIZE];
                while (in.read(buffer) > 0 ) {
                   out.write(buffer);
               } 
           } finally {
                if ( null != in) {
                   in.close();
               } 
                if ( null != out) {
                   out.close();
               } 
           } 
       } catch (Exception e) {
           e.printStackTrace();
       } 
   } 
   
    public static String getExtention(String fileName) {
        int pos = fileName.lastIndexOf(".");
        return fileName.substring(pos);
   }
    
    /**
     * 格式化日期类型
     * @param pattern  需要输出的格式
     * @param date  需要格式的日期
     * @return
     */
    public static String formateDate(String pattern, Date date) {
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		return format.format(date);

	}
    
    /** 传入一个Date，判断是多久前，返回示例"4小时前" */
	public static String getHowLongStr(Date date) {
		String rs = "";
		Long i = date.getTime();
		Date now = new Date();
		Long j = now.getTime();
		long day = 1000 * 60 * 60 * 24;
		long hour = 1000 * 60 * 60;
		long min = 1000 * 60;
		long sec = 1000;
		if (((j - i) / day) > 0)
			rs = ((j - i) / day) + "天前";
		else if (((j - i) / hour) > 0)
			rs = ((j - i) / hour) + "小时前";
		else if (((j - i) / min) > 0)
			rs = ((j - i) / min) + "分钟前";
		else if (((j - i) / sec) > 0)
			rs = ((j - i) / sec) + "秒前";
		return rs;
	}

}
