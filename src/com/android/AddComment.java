package com.android;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.struts2.obj.Comment;
import com.struts2.service.CommentService;

public class AddComment extends HttpServlet {
	
	private CommentService commentService;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		
		PrintWriter pw=null; 
		pw = (PrintWriter)response.getWriter(); 
		
		int appId = Integer.parseInt(request.getParameter("appId"));
		String userName = URLDecoder.decode(request.getParameter("userName"), "utf-8");
		String content = URLDecoder.decode(request.getParameter("content"), "utf-8");
		
		Comment c = new Comment();
		c.setAppId(appId);
		c.setUserName(new String(userName.getBytes("ISO-8859-1"), "utf-8"));
		c.setContent(new String(content.getBytes("ISO-8859-1"), "utf-8"));
		//String x = new String(param.getBytes("ISO-8859-1"), "utf-8"); 
		
		ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		commentService = (CommentService) ctx.getBean("commentService");
		
		commentService.saveComment(c);
		
		pw.print("123");
		
		
	}
}