package com.struts2.action;

import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.struts2.obj.Sign;
import com.struts2.service.SignService;

public class SignAction extends ActionSupport {

	private Sign sign;
	private SignService signService;

	public void setSignService(SignService signService) {
		this.signService = signService;
	}

	public Sign getSign() {
		return sign;
	}

	public void setSign(Sign sign) {
		this.sign = sign;
	}
	
	public String save() throws Exception{
		this.signService.saveSign(sign);
		return "saveSuccess";
	}
	
	public String delete() throws Exception{
		this.signService.removeSign(sign);
		return "deleteSuccess";
	}
	
	@SuppressWarnings("unchecked")
	public String list() throws Exception{
		Map request = (Map)ActionContext.getContext().get("request");
		request.put("ListSign", this.signService.findAllSigns());
		return "listSuccess";
	}
	
	public String show(){
		sign = this.signService.findSignById(sign.getId());
		return "showSuccess";
	}
	
	public List<Sign> findSignBySignType(String signType){
		return this.signService.findSignBySignType(signType);
	}
	
	public List<Sign> findSignByUserId(Integer id){
		return this.signService.findSignByUserId(id);
	}
	
	public String update() throws Exception{
		this.signService.updateSign(sign);
		return "updateSuccess";
	}
}
