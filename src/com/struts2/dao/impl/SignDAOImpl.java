package com.struts2.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.struts2.dao.SignDAO;
import com.struts2.obj.Sign;

public class SignDAOImpl extends HibernateDaoSupport implements SignDAO {

	@Override
	public List<Sign> findAllSigns() {
		String hql = "from Sign order by id desc";
		return (List<Sign>)this.getHibernateTemplate().find(hql);
	}

	@Override
	public Sign findSignById(Integer id) {
		return (Sign)this.getHibernateTemplate().get(Sign.class, id);
	}

	@Override
	public List<Sign> findSignBySignType(String signType) {
		String hql = "from Sign where signType =" + signType + " order by id desc";
		return (List<Sign>)this.getHibernateTemplate().find(hql);
	}

	@Override
	public List<Sign> findSignByUserId(Integer id) {
		String hql = "from Sign where userId =" + id + " order by id desc";
		return (List<Sign>)this.getHibernateTemplate().find(hql);
	}

	@Override
	public void removeSign(Sign sign) {
		this.getHibernateTemplate().delete(sign);

	}

	@Override
	public void saveSign(Sign sign) {
		this.getHibernateTemplate().save(sign);
	}

	@Override
	public void updateSign(Sign sign) {
		this.getHibernateTemplate().saveOrUpdate(sign);

	}

}
