package com.struts2.dao;

import java.util.List;

import com.struts2.obj.Sign;

public interface SignDAO {

	public void saveSign(Sign sign);
	
	public void updateSign(Sign sign);
	
	public void removeSign(Sign sign);
	
	public List<Sign> findAllSigns();
	
	public Sign findSignById(Integer id);
	
	public List<Sign> findSignBySignType(String signType);
	
	public List<Sign> findSignByUserId(Integer id);
}
