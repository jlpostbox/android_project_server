package com.struts2.service.impl;

import java.util.List;

import com.struts2.dao.AppInfoDAO;
import com.struts2.dao.AppSignDAO;
import com.struts2.obj.Appinfo;
import com.struts2.obj.Appsign;
import com.struts2.obj.PageBean;
import com.struts2.obj.Sign;
import com.struts2.service.AppInfoService;

public class AppInfoServiceImpl implements AppInfoService {

	private AppInfoDAO appInfoDao;
	private AppSignDAO appSignDao;

	public AppInfoDAO getAppInfoDao() {
		return appInfoDao;
	}

	public void setAppInfoDao(AppInfoDAO appInfoDao) {
		this.appInfoDao = appInfoDao;
	}

	public AppSignDAO getAppSignDao() {
		return appSignDao;
	}

	public void setAppSignDao(AppSignDAO appSignDao) {
		this.appSignDao = appSignDao;
	}

	@Override
	public List<Appinfo> findAllApps() {
		List<Appinfo> appinfoList = this.appInfoDao.findAllApps();
		for (int i = 0; i < appinfoList.size(); i++) {
			Appinfo ai = appinfoList.get(i);
			List<Sign> signList = appSignDao.getSignByAppId(ai.getId());
			ai.setSign(signList);
		}

		return appinfoList;
	}

	@Override
	public Appinfo findAppById(Integer id) {
		return this.appInfoDao.findAppById(id);
	}

	@Override
	public void removeApp(Appinfo appinfo) {
		this.appInfoDao.removeApp(appinfo);

	}

	@Override
	public void saveApp(Appinfo appinfo, Integer systemId, String appId) {
		this.appInfoDao.saveApp(appinfo);

		Appsign appsign = new Appsign();
		appsign.setAppId(appinfo.getId());
		appsign.setSignId(systemId);

		appSignDao.add(appsign);

		String[] appSigns = appId.split(",");
		for (String appsign1 : appSigns) {
			Appsign appsign2 = new Appsign();
			appsign2.setAppId(appinfo.getId());
			appsign2.setSignId(Integer.parseInt(appsign1.trim()));
			appSignDao.add(appsign2);
		}

		System.out.println("Service层:" + appinfo.getId());
	}

	@Override
	public void updateApp(Appinfo appinfo) {
		this.appInfoDao.updateApp(appinfo);

	}

	@Override
	public PageBean queryForPage(int pageSize, int page) {
		int allRow = appInfoDao.getAllRowCount("from Appinfo");// 总记录数
		int totalPage = PageBean.countTotalPage(pageSize, allRow);// 总页数
		final int offset = PageBean.countOffset(pageSize, page);// 当前页开始记录
		final int length = pageSize;// 每页记录数
		final int currentPage = PageBean.countCurrentPage(page);
		final String hql = "from Appinfo order by id desc";
		List<Appinfo> list = appInfoDao.queryForPage(hql , offset, length);// "一页"的记录
		for (int i = 0; i < list.size(); i++) {
			Appinfo ai = list.get(i);
			List<Sign> signList = appSignDao.getSignByAppId(ai.getId());
			ai.setSign(signList);
		}

		// 把分页信息保存到Bean中
		PageBean pageBean = new PageBean();
		pageBean.setPageSize(pageSize);
		pageBean.setCurrentPage(currentPage);
		pageBean.setAllRow(allRow);
		pageBean.setTotalPage(totalPage);
		pageBean.setList(list);
		pageBean.init();
		return pageBean;
	}
	
	@Override
	public PageBean queryForPageBySign(int pageSize, int page , int signId) {

		String appsignSql = "select appId from Appsign where signId = " + signId;
		final String hql = "from Appinfo where id in (" + appsignSql + ")";
		int allRow = appInfoDao.getAllRowCount(hql);// 总记录数
		int totalPage = PageBean.countTotalPage(pageSize, allRow);// 总页数
		final int offset = PageBean.countOffset(pageSize, page);// 当前页开始记录
		final int length = pageSize;// 每页记录数
		final int currentPage = PageBean.countCurrentPage(page);
		
		List<Appinfo> list = appInfoDao.queryForPage(hql , offset, length);// "一页"的记录
		for (int i = 0; i < list.size(); i++) {
			Appinfo ai = list.get(i);
			List<Sign> signList = appSignDao.getSignByAppId(ai.getId());
			ai.setSign(signList);
		}

		// 把分页信息保存到Bean中
		PageBean pageBean = new PageBean();
		pageBean.setPageSize(pageSize);
		pageBean.setCurrentPage(currentPage);
		pageBean.setAllRow(allRow);
		pageBean.setTotalPage(totalPage);
		pageBean.setList(list);
		pageBean.init();
		return pageBean;
	}

	/**
	 * 增加浏览量
	 */
	@Override
	public void updateView(int appID) {
		appInfoDao.updateView(appID);
		
	}

	/**
	 * 根据浏览量排序所有应用
	 */
	@Override
	public PageBean queryForPageOrderByView(int pageSize, int page) {
		
		int allRow = appInfoDao.getAllRowCount("from Appinfo");// 总记录数
		int totalPage = PageBean.countTotalPage(pageSize, allRow);// 总页数
		final int offset = PageBean.countOffset(pageSize, page);// 当前页开始记录
		final int length = pageSize;// 每页记录数
		final int currentPage = PageBean.countCurrentPage(page);
		final String hql = "from Appinfo order by appView desc";
		List<Appinfo> list = appInfoDao.queryForPage(hql , offset, length);// "一页"的记录
		for (int i = 0; i < list.size(); i++) {
			Appinfo ai = list.get(i);
			List<Sign> signList = appSignDao.getSignByAppId(ai.getId());
			ai.setSign(signList);
		}

		// 把分页信息保存到Bean中
		PageBean pageBean = new PageBean();
		pageBean.setPageSize(pageSize);
		pageBean.setCurrentPage(currentPage);
		pageBean.setAllRow(allRow);
		pageBean.setTotalPage(totalPage);
		pageBean.setList(list);
		pageBean.init();
		return pageBean;
	}

	/**
	 * 搜索功能
	 */
	@Override
	public PageBean search(String key,int pageSize, int page) {
		String hql = "from Appinfo where name like '%" + key + "%' or appDescription like '%"+ key +"%'";
		int allRow = appInfoDao.getAllRowCount(hql);// 总记录数
		int totalPage = PageBean.countTotalPage(pageSize, allRow);// 总页数
		final int offset = PageBean.countOffset(pageSize, page);// 当前页开始记录
		final int length = pageSize;// 每页记录数
		final int currentPage = PageBean.countCurrentPage(page);
		List<Appinfo> list = appInfoDao.queryForPage(hql , offset, length);// "一页"的记录
		for (int i = 0; i < list.size(); i++) {
			Appinfo ai = list.get(i);
			List<Sign> signList = appSignDao.getSignByAppId(ai.getId());
			ai.setSign(signList);
		}

		// 把分页信息保存到Bean中
		PageBean pageBean = new PageBean();
		pageBean.setPageSize(pageSize);
		pageBean.setCurrentPage(currentPage);
		pageBean.setAllRow(allRow);
		pageBean.setTotalPage(totalPage);
		pageBean.setList(list);
		pageBean.init();
		return pageBean;
	}
	
	
}
