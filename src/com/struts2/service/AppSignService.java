package com.struts2.service;

import java.util.List;

import com.struts2.obj.Appsign;
import com.struts2.obj.PageBean;
import com.struts2.obj.Sign;

public interface AppSignService {

	public void add(Appsign appsign);

	public void delete(Appsign appsign);

	public List<Sign> getSignByAppId(Integer id);

	public void update(Appsign appsign);
	
	public PageBean getAllSystemSign(int pageSize, int page);
	
}
