package com.struts2.dao;

import java.util.List;

import com.struts2.obj.Appinfo;
import com.struts2.obj.Appsign;
import com.struts2.obj.Sign;

public interface AppSignDAO {

	public void add(Appsign appsign);

	public void delete(Appsign appsign);

	public List<Sign> getSignByAppId(Integer id);

	public List<Appsign> getSignBySignId(Integer id);

	public void update(Appsign appsign);

	/**
	 * 查询所有记录数
	 * 
	 * @param hql
	 *            查询的条件
	 * @return 总记录数
	 */
	public int getAllRowCount(String hql);

	/**
	 * 　　 * 分页查询 　　 * @param hql 查询的条件 　　 * @param offset 开始记录 　　 * @param
	 * length 一次查询几条记录 　　 * @return 　　
	 */
	public List queryAllDataForPage(final String hql , final int offset, final int length);

}
