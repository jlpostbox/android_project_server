package com.struts2.action;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.struts2.obj.User;
import com.struts2.service.UserService;

public class UserAction extends ActionSupport {

	private User user;
	
	private UserService userService;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	public String save() throws Exception
	{
		userService.saveUser(user);
		return "saveSuccecss";
	}
	
	@SuppressWarnings("unchecked")
	public String list() throws Exception
	{
		Map request = (Map)ActionContext.getContext().get("request");
		request.put("ListUser", userService.findAllUser());
		return "listSuccess";
	}
	public String delete() throws Exception
	{
		this.userService.removeUser(user);
		return "deleteSuccess";
	}
	
	@SuppressWarnings("unchecked")
	public String show() throws Exception
	{
		user = this.userService.findUserById(user.getId());
		return "showSuccess";
	}
	
	public String update() throws Exception
	{
		this.userService.updateUser(user);
		return "updateSuccess";
	}
}
