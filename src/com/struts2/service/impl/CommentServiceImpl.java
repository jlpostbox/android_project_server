package com.struts2.service.impl;

import java.util.List;

import com.struts2.dao.CommentDAO;
import com.struts2.obj.Comment;
import com.struts2.obj.PageBean;
import com.struts2.service.CommentService;

public class CommentServiceImpl implements CommentService {

	private CommentDAO commentdao;
	
	public void setCommentdao(CommentDAO commentdao) {
		this.commentdao = commentdao;
	}

	@Override
	public List<Comment> findByAppID(Integer id) {
		// TODO Auto-generated method stub
		return commentdao.findByAppID(id);
	}

	@Override
	public void removeComment(Comment c) {
		commentdao.removeComment(c);

	}

	@Override
	public void saveComment(Comment c) {
		commentdao.saveComment(c);
	}

	@Override
	public PageBean queryForPage(int pageSize, int page , int appId) {
		String hql = "from Comment where appId = " + appId + "order by id desc";
		int allRow = commentdao.getAllRowCount(hql);// 总记录数
		int totalPage = PageBean.countTotalPage(pageSize, allRow);// 总页数
		final int offset = PageBean.countOffset(pageSize, page);// 当前页开始记录
		final int length = pageSize;// 每页记录数
		final int currentPage = PageBean.countCurrentPage(page);
		List<Comment> list = commentdao.queryForPage(offset, length , hql);// "一页"的记录
		

		// 把分页信息保存到Bean中
		PageBean pageBean = new PageBean();
		pageBean.setPageSize(pageSize);
		pageBean.setCurrentPage(currentPage);
		pageBean.setAllRow(allRow);
		pageBean.setTotalPage(totalPage);
		pageBean.setList(list);
		pageBean.init();
		return pageBean;
	}
}
