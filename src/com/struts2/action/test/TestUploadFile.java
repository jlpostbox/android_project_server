package com.struts2.action.test;

import java.io.File;
import java.util.Date;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.struts2.service.AppInfoService;
import com.tool.Utils;

public class TestUploadFile extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private File appImgFile;
	private String appImgFileContentType;
	private String appImgFileFileName;
	
	private AppInfoService appInfoService;
	
	

	public void setAppInfoService(AppInfoService appInfoService) {
		this.appInfoService = appInfoService;
	}
	public File getAppImgFile() {
		return appImgFile;
	}
	public void setAppImgFile(File appImgFile) {
		this.appImgFile = appImgFile;
	}
	public String getAppImgFileContentType() {
		return appImgFileContentType;
	}
	public void setAppImgFileContentType(String appImgFileContentType) {
		this.appImgFileContentType = appImgFileContentType;
	}
	public String getAppImgFileFileName() {
		return appImgFileFileName;
	}
	public void setAppImgFileFileName(String appImgFileFileName) {
		this.appImgFileFileName = appImgFileFileName;
	}
	
	public String login() throws Exception
	{
		String imageFileName = new Date().getTime() + Utils.getExtention(this.appImgFileFileName);
	    File imageFile = new File(ServletActionContext.getServletContext().getRealPath("UploadImages") + "/" + imageFileName);
	    Utils.copy(this.appImgFile, imageFile);
	    return "success";
	}
	
	public String add() throws Exception
	{
		String imageFileName = new Date().getTime() + Utils.getExtention(this.appImgFileFileName);
	    File imageFile = new File(ServletActionContext.getServletContext().getRealPath("UploadImages") + "/" + imageFileName);
	    Utils.copy(this.appImgFile, imageFile);
	    return "success";
	}
}
