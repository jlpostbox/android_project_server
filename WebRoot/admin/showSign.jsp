<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<title>修改标签</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

	</head>

	<body>
		<h2>
			修改标签
		</h2>

		<s:form action="/admin/updateSign">
			<s:hidden name="sign.id" value="%{sign.id}"></s:hidden>
			<s:textfield name="sign.name" value="%{sign.name}"
				label="%{getText('signName')}"></s:textfield>
			<s:radio name="sign.signType" list="%{#{0:'系统标签',1:'普通标签'}}"
				value="%{sign.signType}" label="%{getText('signType')}" />
			<s:submit></s:submit>
		</s:form>
	</body>
</html>
