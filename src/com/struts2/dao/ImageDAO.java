package com.struts2.dao;

import java.util.List;

import com.struts2.obj.Image;

public interface ImageDAO {
	
	public void saveImage(Image i);
	
	public void removeImage(Image i);
	
	public List<Image> listImage(Integer appId);
}
