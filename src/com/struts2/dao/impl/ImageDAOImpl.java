package com.struts2.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.struts2.dao.ImageDAO;
import com.struts2.obj.Image;

public class ImageDAOImpl extends HibernateDaoSupport implements ImageDAO {

	@Override
	@SuppressWarnings("unchecked")
	public List<Image> listImage(Integer appId) {
		String hql = "from Image where appId = " + appId;
		return (List<Image>)this.getHibernateTemplate().find(hql);
	}

	@Override
	public void removeImage(Image i) {
		this.getHibernateTemplate().delete(i);

	}

	@Override
	public void saveImage(Image i) {
		this.getHibernateTemplate().save(i);

	}

}
