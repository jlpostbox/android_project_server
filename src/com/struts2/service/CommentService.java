package com.struts2.service;

import java.util.List;

import com.struts2.obj.Comment;
import com.struts2.obj.PageBean;

public interface CommentService {

	public void saveComment(Comment c);

	public void removeComment(Comment c);

	public List<Comment> findByAppID(Integer id);
	
	/**
	 * 分页查询
	 * @param currentPage当前第几页
	 * @param pageSize每页大小
	 * @return 封闭了分页信息(包括记录集list)的Bean
	 */
	public PageBean queryForPage(int pageSize, int currentPage , int appId);
}
