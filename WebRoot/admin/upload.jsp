<%@ page language="java" contentType="text/html;charset=gb2312"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<! DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>upload file</title>
		<script type="text/javascript">  
    function addMore() {
        var td = document.getElementById("more");
        var div = document.createElement("div");
        var ss = document.createElement("span");
        var input = document.createElement("input");
        var button = document.createElement("input");
        div.style.marginTop = "4";
        ss.innerHTML = "选择文件: ";
        input.type = "file";
        input.name = "uploadFile";
        button.type = "button";
        button.value = "移 除";
        button.onclick = function() {
            td.removeChild(div);
            div = null;
            fileInputNumber--;
        }
        div.appendChild(ss);
        div.appendChild(input);
        div.appendChild(button);
        td.appendChild(div);
        fileInputNumber++;
    }
    
    function del()
	{
		if(confirm("你真的想删除该记录么？"))
		{
			return true;
		}
		return false;
	}
</script>
	</head>

	<body>
		<h1>
			Upload Images
		</h1>
		<s:fielderror />
		<s:form method="post" name="uploadForm" action="/admin/saveImage"
			enctype="multipart/form-data">
			<input type="hidden" name="i.appId" value="${i.appId }" />
			<div id="more">
				选择文件:
				<input type="file" name="uploadFile" />
				<input type="button" value="添 加" onclick="addMore()" />
			</div>
			<div>
				<s:submit value="上传" />
				<s:reset value="重置" />
			</div>
		</s:form>

		<h1>
			All Images
		</h1>
		<table border="1" width="95%" align="center" style="font: 12px">
			<tr>
				<td>
					ID
				</td>
				<td>
					Image Name
				</td>
				<td>
					Application ID
				</td>
				<td>
					Operation
				</td>
			</tr>
			<s:iterator value="#request.ListImage" id="images">
				<tr>
					<td>
						${images.id }
					</td>
					<td>
						${images.name }
					</td>
					<td>
						${images.appId }
					</td>
					<td>
						<s:a href="removeImage.action?i.id=%{#images.id}&i.appId=%{#images.appId}" onclick="return del();">Delete</s:a>
					</td>
				</tr>
			</s:iterator>
		</table>
	</body>
</html>