<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<title>Add Sign</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript">
	function del()
	{
		if(confirm("你真的想删除该记录么？"))
		{
			return true;
		}
		return false;
	}
	</script>
	</head>

	<body>
		<h2>
			Add Sign
		</h2>

		<s:form action="/admin/saveSign">
			<s:textfield name="sign.name" label="%{getText('signName')}"></s:textfield>
			<s:radio name="sign.signType" list="%{#{0:'系统标签',1:'普通标签'}}"
				label="%{getText('signType')}" />
			<s:submit></s:submit>
		</s:form>

		<h1>
			All Application
		</h1>

		<table border="1" width="90%" align="center" style="font: 12px">
			<tr>
				<td>
					序号
				</td>
				<td>
					软件名
				</td>
				<td>
					标签
				</td>
				<td>
					操作
				</td>
			</tr>
			<s:iterator value="#request.ListSign" id="sign">
				<tr>
					<td>
						<s:property value="#sign.id" />
					</td>
					<td>
						<s:property value="#sign.name" />
					</td>
					<td>
						<s:if test="#sign.signType == 1">普通标签</s:if>
						<s:if test="#sign.signType == 0">系统标签</s:if>
					</td>
					<td>
						<s:a href="showSign.action?sign.id=%{#sign.id}">更新</s:a>
						<s:a href="deleteSign.action?sign.id=%{#sign.id}"
							onclick="return del();">删除</s:a>
					</td>
				</tr>
			</s:iterator>
		</table>
	</body>
</html>
