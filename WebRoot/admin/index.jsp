<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>My JSP 'index.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  </head>
  
  <body>
    <h1>Operation List</h1>
    
    <s:a href="admin/saveUser.jsp">Save User</s:a>   
    <s:a href="admin/listUser.action">List Users</s:a><br/><br>
    
    <s:a href="admin/listApps.action">List Apps</s:a>
    
    <s:a href="admin/listSign.action">List Sign</s:a>
  </body>
</html>
