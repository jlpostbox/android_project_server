package com.struts2.service.impl;

import java.util.List;

import com.struts2.dao.ImageDAO;
import com.struts2.obj.Image;
import com.struts2.service.ImageService;

public class ImageServiceImpl implements ImageService {

	

	private ImageDAO imagedao;
	
	public void setImagedao(ImageDAO imagedao) {
		this.imagedao = imagedao;
	}

	@Override
	public void removeImage(Image i) {
		imagedao.removeImage(i);

	}

	@Override
	public void saveImage(Image i) {
		imagedao.saveImage(i);

	}
	
	@Override
	public List<Image> listImage(Integer appId) {
		return imagedao.listImage(appId);
	}

}
