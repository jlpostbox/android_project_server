package com.android;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.struts2.obj.Image;
import com.struts2.service.ImageService;

public class GetAllImages extends HttpServlet {
	
	private ImageService imageService;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		
		PrintWriter pw=null; 
		pw = (PrintWriter)response.getWriter(); 
		
		int appId = Integer.parseInt(request.getParameter("appId"));
		//String x = new String(param.getBytes("ISO-8859-1"), "utf-8"); 
		
		ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		imageService = (ImageService) ctx.getBean("imageService");
		
		List<Image> imageList = imageService.listImage(appId);
		
		JSONArray jsonArray = JSONArray.fromObject(imageList);
		//JSONObject jsonObject = JSONObject.fromObject(pageBean);
		System.out.println(jsonArray);
		
		pw.print(jsonArray);
		
		
	}
}