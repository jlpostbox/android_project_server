<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<title>所有用户</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript">
	function del()
	{
		if(confirm("你真的想删除该记录么？"))
		{
			return true;
		}
		return false;
	}
	</script>
	</head>

	<body>
		<h1>
			All User
		</h1>

		<table border="1" width="80%" align="center">
			<tr>
				<td>
					序号
				</td>
				<td>
					姓名
				</td>
				<td>
					密码
				</td>
				<td>
					帐号类型
				</td>
				<td>
					帐号状态
				</td>
				<td>
					操作
				</td>
			</tr>
			<s:iterator value="#request.ListUser" id="user">
				<tr>
					<td>
						<s:property value="#user.id"/>
					</td>
					<td>
						<s:property value="#user.name"/>
					</td>
					<td>
						<s:property value="#user.password"/>
					</td>
					<td>
						<s:if test="#user.userType == 1">管理员</s:if>
						<s:if test="#user.userType == 0">普通用户</s:if>
					</td>
					<td>
						<s:if test="#user.userStatus == 1">正常</s:if>
						<s:if test="#user.userStatus == 0">禁用</s:if>
					</td>
					<td>
						<s:a href="showUser.action?user.id=%{#user.id}">更新</s:a>
						<s:a href="deleteUser.action?user.id=%{#user.id}" onclick="return del();">删除</s:a>
					</td>
				</tr>
			</s:iterator>
		</table>
	</body>
</html>
