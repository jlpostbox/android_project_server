package com.struts2.action;

import java.io.File;
import java.util.Date;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.struts2.obj.Appinfo;
import com.struts2.obj.PageBean;
import com.struts2.service.AppInfoService;
import com.struts2.service.SignService;
import com.tool.Utils;

@SuppressWarnings("unchecked")
public class AppInfoAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private Appinfo appInfo;
	private AppInfoService appInfoService;
	private SignService signService;

	private Integer systemId;
	private String appId;
	private Integer signId;

	private int page;// 第几页
	private PageBean pageBean;// 包含分布信息的bean

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public PageBean getPageBean() {
		return pageBean;
	}

	public void setPageBean(PageBean pageBean) {
		this.pageBean = pageBean;
	}

	// 文件上传
	private File appImgFile;
	private String appImgFileContentType;
	private String appImgFileFileName;

	// s:file/>标志不仅仅是绑定到myFile，还有myFileContentType（上传文件的MIME类型）和myFileFileName（上传文件的文件名，该文件名不包括文件的路径）
	public File getAppImgFile() {
		return appImgFile;
	}

	public void setAppImgFile(File appImgFile) {
		this.appImgFile = appImgFile;
	}

	public String getAppImgFileContentType() {
		return appImgFileContentType;
	}

	public void setAppImgFileContentType(String appImgFileContentType) {
		this.appImgFileContentType = appImgFileContentType;
	}

	public String getAppImgFileFileName() {
		return appImgFileFileName;
	}

	public void setAppImgFileFileName(String appImgFileFileName) {
		this.appImgFileFileName = appImgFileFileName;
	}

	public void setAppInfo(Appinfo appInfo) {
		this.appInfo = appInfo;
	}

	public Appinfo getAppInfo() {
		return appInfo;
	}

	public void setAppInfoService(AppInfoService appInfoService) {
		this.appInfoService = appInfoService;
	}

	public SignService getSignService() {
		return signService;
	}

	public void setSignService(SignService signService) {
		this.signService = signService;
	}

	public Integer getSystemId() {
		return systemId;
	}

	public void setSystemId(Integer systemId) {
		this.systemId = systemId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public Integer getSignId() {
		return signId;
	}

	public void setSignId(Integer signId) {
		this.signId = signId;
	}

	public String add() throws Exception {
		String imageFileName = new Date().getTime()
				+ Utils.getExtention(this.appImgFileFileName);
		appInfo.setAppImg(imageFileName);
		File imageFile = new File(ServletActionContext.getServletContext()
				.getRealPath("UploadImages")
				+ "/" + imageFileName);
		Utils.copy(this.appImgFile, imageFile);
		appInfoService.saveApp(appInfo, systemId, appId);
		return "addSuccess";

	}

	public String list() throws Exception {
		Map request = (Map) ActionContext.getContext().get("request");

		request.put("ListSignType0", signService.findSignBySignType("0"));
		request.put("ListSignType1", signService.findSignBySignType("1"));

		// List<Appinfo> appinfolist = appInfoService.findAllApps();
		// 分页的pageBean,参数pageSize表示每页显示记录数,page为当前页
		this.pageBean = appInfoService.queryForPage(10, page);

		request.put("ListApps", pageBean);
		return "listSuccess";
	}

	public String delete() throws Exception {
		this.appInfoService.removeApp(appInfo);
		return "deleteSuccess";
	}

	public String show() throws Exception {
		Map request = (Map) ActionContext.getContext().get("request");

		request.put("ListSignType0", signService.findSignBySignType("0"));
		request.put("ListSignType1", signService.findSignBySignType("1"));
		appInfo = this.appInfoService.findAppById(appInfo.getId());
		return "showSuccess";
	}

	public String update() throws Exception {
		String imageFileName = new Date().getTime()
				+ Utils.getExtention(this.appImgFileFileName);
		appInfo.setAppImg(imageFileName);
		File imageFile = new File(ServletActionContext.getServletContext()
				.getRealPath("UploadImages")
				+ "/" + imageFileName);
		Utils.copy(this.appImgFile, imageFile);

		this.appInfoService.updateApp(appInfo);
		return "updateSuccecss";
	}

	public String listBySign() throws Exception {
		Map request = (Map) ActionContext.getContext().get("request");
		request.put("ListSignType0", signService.findSignBySignType("0"));
		request.put("ListSignType1", signService.findSignBySignType("1"));
		this.pageBean = this.appInfoService
				.queryForPageBySign(10, page, signId);
		request.put("ListApps", pageBean);
		return "listSuccess";
	}
}
