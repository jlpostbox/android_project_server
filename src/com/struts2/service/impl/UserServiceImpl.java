package com.struts2.service.impl;

import java.util.List;

import com.struts2.dao.UserDAO;
import com.struts2.obj.User;
import com.struts2.service.UserService;

public class UserServiceImpl implements UserService {

	private UserDAO userdao;

	public void setUserdao(UserDAO userdao) {
		this.userdao = userdao;
	}

	public List<User> findAllUser() {		
		return this.userdao.findAllUsers();
	}

	public User findUserById(Integer id) {
		
		return this.userdao.findUserById(id);
	}

	public void removeUser(User user) {
		this.userdao.removeUser(user);

	}

	public void saveUser(User user) {
		this.userdao.saveUser(user);

	}

	public void updateUser(User user) {
		this.userdao.updateUser(user);

	}

}
