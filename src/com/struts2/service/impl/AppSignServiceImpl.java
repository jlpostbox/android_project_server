package com.struts2.service.impl;

import java.util.List;

import com.struts2.dao.AppSignDAO;
import com.struts2.obj.Appinfo;
import com.struts2.obj.Appsign;
import com.struts2.obj.PageBean;
import com.struts2.obj.Sign;
import com.struts2.service.AppSignService;

public class AppSignServiceImpl implements AppSignService {

	private AppSignDAO appSignDao;
	
	
	public void setAppSignDao(AppSignDAO appSignDao) {
		this.appSignDao = appSignDao;
	}

	@Override
	public void add(Appsign appsign) {
		appSignDao.add(appsign);
		
	}

	@Override
	public void delete(Appsign appsign) {
		appSignDao.delete(appsign);

	}

	@Override
	public List<Sign> getSignByAppId(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Appsign appsign) {
		appSignDao.update(appsign);

	}

	@Override
	public PageBean getAllSystemSign(int pageSize, int page) {
		
		final StringBuffer sb = new StringBuffer();
		sb.append("select st.id,st.name,ait.app_img ");
		sb.append("from signtable as st, ");
		sb.append("appsigntable as ast, ");
		sb.append("appinfotable as ait ");
		sb.append("where sign_type = 0 and st.id = ast.sign_id and ait.id = ast.app_id group by st.name");
		
		int allRow = appSignDao.getAllRowCount(sb.toString());// 总记录数
		int totalPage = PageBean.countTotalPage(pageSize, allRow);// 总页数
		final int offset = PageBean.countOffset(pageSize, page);// 当前页开始记录
		final int length = pageSize;// 每页记录数
		final int currentPage = PageBean.countCurrentPage(page);
		
		List list = appSignDao.queryAllDataForPage(sb.toString(),offset,length);// "一页"的记录
		

		// 把分页信息保存到Bean中
		PageBean pageBean = new PageBean();
		pageBean.setPageSize(pageSize);
		pageBean.setCurrentPage(currentPage);
		pageBean.setAllRow(allRow);
		pageBean.setTotalPage(totalPage);
		pageBean.setList(list);
		pageBean.init();
		return pageBean;
	}

}
