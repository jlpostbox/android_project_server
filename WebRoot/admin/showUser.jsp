<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<title>修改用户</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

	</head>

	<body>
		<h2>
			修改用户
		</h2>

		<s:form action="/admin/updateUser">
			<s:hidden name="user.id" value="%{user.id}"></s:hidden>
			<s:textfield name="user.name" value="%{user.name}"
				label="%{getText('username')}"></s:textfield>
			<s:textfield name="user.password" value="%{user.password}"
				label="%{getText('password')}"></s:textfield>

			<s:radio name="user.userType" list="%{#{0:'普通用户',1:'管理员'}}" value="%{user.userType}" label="%{getText('userType')}"/>
			<s:radio name="user.userStatus" list="%{#{0:'禁用',1:'正常'}}" value="%{user.userStatus}" label="%{getText('userStatus')}"/>


			<s:submit></s:submit>
		</s:form>
	</body>
</html>
