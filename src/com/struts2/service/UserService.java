package com.struts2.service;

import java.util.List;

import com.struts2.obj.User;

public interface UserService {

	public void saveUser(User user);
	
	public void removeUser(User user);
	
	public void updateUser(User user);
	
	public List<User> findAllUser();
	
	public User findUserById(Integer id);
}
