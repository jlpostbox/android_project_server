package com.struts2.action;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.struts2.obj.Appinfo;
import com.struts2.obj.Comment;
import com.struts2.obj.PageBean;
import com.struts2.service.AppInfoService;
import com.struts2.service.CommentService;

public class CommentAction extends ActionSupport {

	private CommentService cs;
	private Comment c;
	private AppInfoService appInfoService;
	private Appinfo appInfo;
	
	private int page;//第几页  
	private PageBean pageBean;//包含分布信息的bean

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public PageBean getPageBean() {
		return pageBean;
	}

	public void setPageBean(PageBean pageBean) {
		this.pageBean = pageBean;
	}

	public void setCs(CommentService cs) {
		this.cs = cs;
	}

	public Comment getC() {
		return c;
	}

	public void setC(Comment c) {
		this.c = c;
	}

	
	public Appinfo getAppInfo() {
		return appInfo;
	}

	public void setAppInfo(Appinfo appInfo) {
		this.appInfo = appInfo;
	}

	public void setAppInfoService(AppInfoService appInfoService) {
		this.appInfoService = appInfoService;
	}

	public String save() throws Exception {
		cs.saveComment(c);
		return "saveSuccess";
	}

	public String remove() throws Exception {
		cs.removeComment(c);
		return "removeSuccess";
	}

	@SuppressWarnings("unchecked")
	public String list() throws Exception {
		Map request = (Map)ActionContext.getContext().get("request");
		this.pageBean = cs.queryForPage(2, page , c.getAppId());
		this.appInfo = appInfoService.findAppById(c.getAppId());
		request.put("ListComment", pageBean);
		request.put("appInfo",appInfo);
		return "listSuccess";
	}
}
