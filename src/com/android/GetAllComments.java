package com.android;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.struts2.obj.PageBean;
import com.struts2.service.CommentService;
import com.tool.CommentJsonValueProcessor;

public class GetAllComments extends HttpServlet {
	
	private CommentService commentService;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		
		PrintWriter pw=null; 
		pw = (PrintWriter)response.getWriter(); 
		
		int page = Integer.parseInt(request.getParameter("page"));
		int appId = Integer.parseInt(request.getParameter("appId"));
		//String x = new String(param.getBytes("ISO-8859-1"), "utf-8"); 
		System.out.println(page); 
		
		ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		commentService = (CommentService) ctx.getBean("commentService");
		
		PageBean pageBean = commentService.queryForPage(10, page ,appId);
		
		//创建了我自己定义的processor对象
        JsonValueProcessor jsonProcessor = new CommentJsonValueProcessor();
        JsonConfig jsonConfig = new JsonConfig();
        //这步是关键，给JsonConfig注册我自己写的JsonValueProcessor
        jsonConfig.registerJsonValueProcessor(Date.class, jsonProcessor);

        JSONObject jsonObject = JSONObject.fromObject(pageBean, jsonConfig);
        //然后利用JSONSerializer的toJSON方法生成JSON对象，注意参数有2个，第二个就是注册过PersonJsonValueProcessor的JsonConfig
        //JSON json = JSONSerializer.toJSON(pageBean, jsonConfig);
		
		pw.print(jsonObject);
		
		
	}
}