package com.android;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.struts2.service.AppInfoService;

public class UpdateAppView extends HttpServlet {

	private AppInfoService appInfoService;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		
		int appID = Integer.parseInt(request.getParameter("appID"));
		ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		appInfoService = (AppInfoService) ctx.getBean("appInfoService");
		
		appInfoService.updateView(appID);
		
	}
}
