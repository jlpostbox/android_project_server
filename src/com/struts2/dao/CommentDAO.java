package com.struts2.dao;

import java.util.List;

import com.struts2.obj.Comment;

public interface CommentDAO {

	public void saveComment(Comment c);
	
	public void removeComment(Comment c);
	
	public List<Comment> findByAppID(Integer id);
	
	/**
　　 * 分页查询 
　　 * @param hql 查询的条件 
　　 * @param offset 开始记录 
　　 * @param length 一次查询几条记录 
　　 * @return 
　　 */  
	public List<Comment> queryForPage(final int offset,final int length , final String hql);
	
	/**
	* 查询所有记录数 
	* @param hql 查询的条件 
	* @return 总记录数 
	*/  
	public int getAllRowCount(final String hql);
}
