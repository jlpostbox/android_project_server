package com.struts2.obj;

/**
 * Imageclass entity. @author MyEclipse Persistence Tools
 */

public class Image implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;
	private Integer appId;

	// Constructors

	/** default constructor */
	public Image() {
	}

	/** full constructor */
	public Image(String name, Integer appId) {
		this.name = name;
		this.appId = appId;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAppId() {
		return this.appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

}