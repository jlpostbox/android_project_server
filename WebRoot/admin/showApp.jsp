<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<title>Update Application</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

	</head>

	<body>
		<h2>
			Update Application
		</h2>
		<s:fielderror />
		<s:form action="/admin/updateApps" method="post"
			enctype="multipart/form-data">
			<s:hidden name="appInfo.id" value="%{appInfo.id}"></s:hidden>
			<s:textfield name="appInfo.name" value="%{appInfo.name}"
				label="%{getText('appName')}" />
			<s:textarea name="appInfo.appDescription" value="%{appInfo.appDescription}"
				label="%{getText('appDescription')}" />
			<s:textfield name="appInfo.appDownAddress" value="%{appInfo.appDownAddress}"
				label="%{getText('appDownAddress')}"></s:textfield>
			<s:textfield name="appInfo.appType" value="%{appInfo.appType}"
				label="%{getText('appType')}"></s:textfield>
			<s:textfield name="appInfo.appUseEnvironment" value="%{appInfo.appUseEnvironment}"
				label="%{getText('userEnvironment')}"></s:textfield>
			<s:textfield name="appInfo.appSystemRequest" value="%{appInfo.appSystemRequest}"
				label="%{getText('appSystemRequest')}"></s:textfield>
			<s:file name="appImgFile" label="%{getText('appImg')}" />
			<img src="../UploadImages/<s:property value='%{appInfo.appImg}' />" width='300' />
			<s:radio name="systemId" list="#request.ListSignType0" value="%{appInfo.sign.id}" listKey="id" listValue="name" label="%{getText('systemSign')}" />
			<s:checkboxlist name="appId" list="#request.ListSignType1" listKey="id" listValue="name" label="%{getText('appSign')}" />
			<s:submit></s:submit>
		</s:form>
	</body>
</html>
