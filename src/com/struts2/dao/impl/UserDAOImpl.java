package com.struts2.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.struts2.dao.UserDAO;
import com.struts2.obj.User;

public class UserDAOImpl extends HibernateDaoSupport implements UserDAO {

	@SuppressWarnings("unchecked")
	public List<User> findAllUsers() {
		String hql = "from User order by id desc";
		return (List<User>)this.getHibernateTemplate().find(hql);
	}

	public User findUserById(Integer id) {		
		return (User)this.getHibernateTemplate().get(User.class, id);
	}

	public void removeUser(User user) {
		this.getHibernateTemplate().delete(user);

	}

	public void saveUser(User user) {
		this.getHibernateTemplate().save(user);
	}

	public void updateUser(User user) {
		this.getHibernateTemplate().saveOrUpdate(user);
	}

}
