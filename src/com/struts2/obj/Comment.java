package com.struts2.obj;

import java.util.Date;

/**
 * Commenttable entity. @author MyEclipse Persistence Tools
 */

public class Comment implements java.io.Serializable {

	// Fields

	private Integer id;
	private String content;
	private Date commentTime = new Date();
	private Integer userId;
	private String userName;
	private Integer appId;

	// Constructors

	/** default constructor */
	public Comment() {
	}

	/** minimal constructor */
	public Comment(String content, Date commentTime, Integer appId) {
		this.content = content;
		this.commentTime = commentTime;
		this.appId = appId;
	}

	/** full constructor */
	public Comment(String content, Date commentTime, Integer userId,
			String userName, Integer appId) {
		this.content = content;
		this.commentTime = commentTime;
		this.userId = userId;
		this.userName = userName;
		this.appId = appId;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCommentTime() {
		return commentTime;
	}

	public void setCommentTime(Date commentTime) {
		this.commentTime = commentTime;
	}

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getAppId() {
		return this.appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

}