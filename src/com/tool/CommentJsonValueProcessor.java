package com.tool;

import java.util.Date;

import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

public class CommentJsonValueProcessor implements JsonValueProcessor {

	@Override
	public Object processArrayValue(Object target, JsonConfig arg1) {
		return process(target);

	}

	@Override
	public Object processObjectValue(String arg0, Object target, JsonConfig arg2) {
		return process(target);

	}

	private String process(Object target){
        if(target instanceof Date){       	
        	String newStringDate = Utils.formateDate("HH:mm:ss", (Date)target);
            return Utils.getHowLongStr((Date)target) + " " + newStringDate; 
        }else{
            return "";
        }
    }

}
