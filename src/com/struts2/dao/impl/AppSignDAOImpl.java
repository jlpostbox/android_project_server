package com.struts2.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.struts2.dao.AppSignDAO;
import com.struts2.obj.Appinfo;
import com.struts2.obj.Appsign;
import com.struts2.obj.Sign;

@SuppressWarnings("unchecked")
public class AppSignDAOImpl extends HibernateDaoSupport implements AppSignDAO {

	@Override	
	public List<Sign> getSignByAppId(Integer id) {
		String hql = "SELECT si FROM Appsign as ap , Sign as si WHERE ap.signId = si.id AND ap.appId = "
				+ id;
		return (List<Sign>) this.getHibernateTemplate().find(hql);
	}

	@Override
	public List<Appsign> getSignBySignId(Integer id) {
		String hql = "from Appsign where signId = " + id;
		return (List<Appsign>) this.getHibernateTemplate().find(hql);
	}

	@Override
	public void add(Appsign appsign) {
		this.getHibernateTemplate().save(appsign);

	}

	@Override
	public void delete(Appsign appsign) {
		this.getHibernateTemplate().delete(appsign);

	}

	@Override
	public void update(Appsign appsign) {
		this.getHibernateTemplate().update(appsign);

	}

	@Override
	public int getAllRowCount(final String hql) {
		List l = getHibernateTemplate().executeFind(new HibernateCallback(){   
			public Object doInHibernate(Session session) throws HibernateException,SQLException{   
				Query query = session.createSQLQuery(hql);     
				List list = query.list();
				return list;
			}   
		});
		
		
		return l.size();
	}

	@Override
	public List queryAllDataForPage(final String hql , final int offset, final int length) {
		// TODO Auto-generated method stub
		List l = getHibernateTemplate().executeFind(new HibernateCallback(){   
			public Object doInHibernate(Session session) throws HibernateException,SQLException{   
				Query query = session.createSQLQuery(hql);   
				query.setFirstResult(offset);   
				query.setMaxResults(length);   
				List list = query.list();
				return list;
			}   
		});
		return l;
	}
	
	

}
